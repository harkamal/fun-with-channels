package envutil

import (
	"fmt"
	"os"
	"testing"
)

func TestGetEnvOptionalString(t *testing.T) {
	tests := []struct {
		name           string
		envKey         string
		defaultValue   string
		expectedResult string
	}{
		{
			name:           "WithDefaultValue",
			envKey:         "NON_EXISTING_KEY",
			defaultValue:   "4000",
			expectedResult: "4000",
		},
		{
			name:           "WithEnvValue",
			envKey:         "PORT",
			defaultValue:   "4000",
			expectedResult: "3000",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Set environment variable if needed
			if tt.envKey != "NON_EXISTING_KEY" {
				os.Setenv(tt.envKey, tt.expectedResult)
				defer os.Unsetenv(tt.envKey)
			}

			result := GetEnvOptionalString(tt.envKey, tt.defaultValue)

			if result != tt.expectedResult {
				t.Errorf("OptionalString returned incorrect value, got: %s, want: %s.", result, tt.expectedResult)
			}
		})
	}
}

func TestGetEnvOptionalInt(t *testing.T) {
	tests := []struct {
		name           string
		envKey         string
		defaultValue   int
		expectedResult int
	}{
		{
			name:           "DefaultValue",
			envKey:         "NON_EXISTING_KEY",
			defaultValue:   40,
			expectedResult: 40,
		},
		{
			name:           "EnvValue",
			envKey:         "PORT",
			defaultValue:   40,
			expectedResult: 50,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Set environment variable if needed
			if tt.envKey != "NON_EXISTING_KEY" {
				os.Setenv(tt.envKey, fmt.Sprint(tt.expectedResult))
				defer os.Unsetenv(tt.envKey)
			}

			result := GetEnvOptionalInt(tt.envKey, tt.defaultValue)

			if result != tt.expectedResult {
				t.Errorf("OptionalString returned incorrect value, got: %d, want: %d.", result, tt.expectedResult)
			}
		})
	}
}
