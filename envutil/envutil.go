package envutil

import (
	"log"
	"os"
	"strconv"
)

func GetEnvOptionalString(key, defVal string) string {
	val, ex := os.LookupEnv(key)
	if !ex {
		return defVal
	}
	return val
}

func GetEnvOptionalInt(key string, defVal int) int {
	val, ex := os.LookupEnv(key)
	if !ex {
		return defVal
	}
	ret, err := strconv.Atoi(val)
	if err != nil {
		log.Fatal("unexpected value of key: ", key)
	}
	return ret
}
