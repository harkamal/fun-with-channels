package cryptomodel

type FinnhubDataWrapper struct {
	Data []FinnhubTradeData `json:"data"`
	Type string             `json:"type"`
}

type FinnhubTradeData struct {
	Price     float64 `json:"p"`
	Symbol    string  `json:"s"`
	Timestamp int64   `json:"t"`
}
