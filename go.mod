module gitlab.com/challenges/fun-with-channels/v2

go 1.15

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/gorilla/websocket v1.4.2
	github.com/lib/pq v1.10.9
)
