package main

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq"
	"gitlab.com/challenges/fun-with-channels/v2/config"
	"gitlab.com/challenges/fun-with-channels/v2/datapipeline"
)

var db *sql.DB

func setupDB(config *config.Config) (*sql.DB, error) {
	var err error
	log.Println("Connecting to database...")
	db, err = sql.Open("postgres", config.DBURL)
	if err != nil {
		panic(err)
	}

	// Create users table
	if _, err := db.Exec(`
		CREATE TABLE IF NOT EXISTS finnhub_moving_averages (
			symbol VARCHAR(255),
			moving_average float,
			timestamp bigint
		);
	`); err != nil {
		log.Fatal(err)
	}
	return db, nil
}

func main() {
	config, err := config.New()
	if err != nil {
		panic("failed to initialize config")
	}
	db, err = setupDB(config)
	if err != nil {
		log.Fatal("could not connect to database: ", err)
	}
	defer db.Close()
	log.Println("Starting datapipeline...")
	datapipeline.NewDataPipeline(config).StartFinnHubDataPipeline(db)
}
