.PHONY: test
test: 
	go test ./...

.PHONY: run
run: 
	go mod tidy
	go run .

.PHONY: build
build:
	go build -o bin/main main.go

.PHONY: up
up:
	docker-compose up