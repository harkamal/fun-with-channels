package datapipeline

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"

	"github.com/gorilla/websocket"
	"gitlab.com/challenges/fun-with-channels/v2/config"
	"gitlab.com/challenges/fun-with-channels/v2/cryptomodel"
	"gitlab.com/challenges/fun-with-channels/v2/datagenerator"
)

const (
	// Crypto symbols to subscribe
	BinanceBTCUSDT  = "BINANCE:BTCUSDT"
	BinanceAETHUSDT = "BINANCE:ETHUSDT"
	BinanceADAUSDT  = "BINANCE:ADAUSDT"
)

var finnhubSymbols = []string{BinanceBTCUSDT, BinanceAETHUSDT, BinanceADAUSDT}

// DataPipeline holds the components of the data pipeline.
type DataPipeline struct {
	Config        *config.Config
	DataChannel   chan cryptomodel.FinnhubTradeData
	FinnhubWSConn *websocket.Conn
}

// NewDataPipeline initializes a new DataPipeline instance.
func NewDataPipeline(config *config.Config) *DataPipeline {
	return &DataPipeline{
		Config: config,
		// finnhubDataChannel channel to send and receive trade updates in goroutines
		DataChannel: make(chan cryptomodel.FinnhubTradeData),
		// WS connection should be initialized when connecting to Finnhub
		FinnhubWSConn: nil,
	}
}

func (dp *DataPipeline) StartFinnHubDataPipeline(db *sql.DB) {
	// create a websocket connection to finnhub
	dp.connectToFinnhub()
	defer dp.FinnhubWSConn.Close()

	// subscribe to finnhubSymbols
	dp.subscribeToFinnhubSymbols()

	// listen finhubb trade update concurrently
	go dp.listenForFinnhubUpdates()

	// process finhubb trade update concurrently
	findhubDataGeneraor := datagenerator.NewFinnhubDataGenerator(dp.Config.FinnhubTradeWindowSize, db)
	findhubDataGeneraor.ProcessFinnhubTradeData(dp.DataChannel)
}

func (dp *DataPipeline) connectToFinnhub() {
	log.Printf("conneting to finnhub %s", dp.Config.FinnhubWebSocketURL)
	wsURL := fmt.Sprintf("%s?token=%s", dp.Config.FinnhubWebSocketURL, dp.Config.FinnhubApiKey)
	w, _, err := websocket.DefaultDialer.Dial(wsURL, nil)
	if err != nil {
		panic(err)
	}
	dp.FinnhubWSConn = w
}

func (dp *DataPipeline) subscribeToFinnhubSymbols() {
	for _, s := range finnhubSymbols {
		log.Printf("subcribing symbos %s", s)
		msg, _ := json.Marshal(map[string]interface{}{"type": "subscribe", "symbol": s})

		err := dp.FinnhubWSConn.WriteMessage(websocket.TextMessage, msg)
		if err != nil {
			panic(err)
		}
	}
}

func (dp *DataPipeline) listenForFinnhubUpdates() {
	var finnhubDataWrapper cryptomodel.FinnhubDataWrapper
	log.Printf("receving live updates")
	for {

		err := dp.FinnhubWSConn.ReadJSON(&finnhubDataWrapper)
		if err != nil {
			panic(err)
		}

		switch finnhubDataWrapper.Type {
		case "trade":
			for _, tradeData := range finnhubDataWrapper.Data {
				dp.DataChannel <- tradeData
			}
		}
	}
}
