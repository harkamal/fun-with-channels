# Fun-With-Channls

A simple concurrent datapipeline, subscribes to real time trades from Finnhub stock API. Simple moving average(SMA) is calculated for the given window, default 60. SMA is also stored in postgres DB as well as latest in memory.



## Getting started

- Fun-With-Channls is a Go project written in GO1.15
- Data pipeline connects to Finnhub via websocket using "gorilla/websocket".
- Requires API key to connect to Finnhub
### Project setup

**Prerequisites**

- Go 1.15

**Clone the repo**

Clone the repo

```bash
git clone https://gitlab.com/harkamal/fun-with-channels.git
```

### Compile and start

```bash
export DATABASE_URL="MY_DATABASE_URL"
export FINNHUB_API_KEY="MY_FINNHUB_API_KEY"
make run
```

### Run tests

```bash
make test
```

## Running in Docker

Follow below steps to run the datapipeline inside docker.

```bash
git clone https://gitlab.com/harkamal/fun-with-channels.git
```

```bash
cd fun-with-channels
```

```bash
make up 
```
