// finnhub_repository_test.go

package repo

import (
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"gitlab.com/challenges/fun-with-channels/v2/cryptomodel"
)

func TestFinnhubRepository_InsertMovingAverage(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	repo := NewFinnhubRepository(db)

	// Mock database expectations
	trade := cryptomodel.FinnhubTradeData{
		Symbol:    "BTCUSD",
		Price:     50000.0,
		Timestamp: time.Now().UnixNano(),
	}
	sma := 3.00
	mock.ExpectBegin()
	mock.ExpectExec("INSERT INTO finnhub_moving_averages").WithArgs(trade.Symbol, sma, trade.Timestamp).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()
	err = repo.InsertMovingAverage(sma, trade)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}
