package repo

import (
	"database/sql"

	"gitlab.com/challenges/fun-with-channels/v2/cryptomodel"
)

const (
	InsertMovingAverageQuery = `INSERT INTO finnhub_moving_averages (symbol, moving_average, timestamp) VALUES ($1, $2, $3)`
)

// FinnhubRepository defines methods for interacting with the finnhub table.
type FinnhubRepository interface {
	InsertMovingAverage(movingAverage float64, trade cryptomodel.FinnhubTradeData) error
}

// finnhubRepository implements FinnhubRepository.
type finnhubRepository struct {
	db *sql.DB
}

// NewUserRepository creates a new FinnhubRepository instance.
func NewFinnhubRepository(db *sql.DB) FinnhubRepository {
	return &finnhubRepository{db}
}

// InsertTrade create a trade
func (r *finnhubRepository) InsertMovingAverage(movingAverage float64, trade cryptomodel.FinnhubTradeData) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	defer func() {
		switch err {
		case nil:
			err = tx.Commit()
		default:
			tx.Rollback()
		}
	}()
	if _, err := tx.Exec(InsertMovingAverageQuery, trade.Symbol, movingAverage, trade.Timestamp); err != nil {
		return err
	}
	return nil
}
