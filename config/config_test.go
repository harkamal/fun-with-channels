package config

import (
	"os"
	"testing"
)

func TestNewConfig(t *testing.T) {
	tests := []struct {
		name                        string
		envFinnhubWSURL             string
		envFinnhubAPIKey            string
		envFinnhubTradeWindowSize   string
		expectedFinnhubWSURL        string
		expectedFinnhubAPIKey       string
		expectedFinnhubTradeWinSize int
		setEnvVariable              bool
	}{
		{
			name:                        "With default values",
			expectedFinnhubWSURL:        DefaultFinnHubWSURL,
			expectedFinnhubAPIKey:       DefaultFinnhubApiKey,
			expectedFinnhubTradeWinSize: DefaultFinnhubTradeWindowSize,
			setEnvVariable:              false,
		},
		{
			name:                        "With env values",
			envFinnhubWSURL:             "wss://custom-ws-url",
			envFinnhubAPIKey:            "custom-api-key",
			envFinnhubTradeWindowSize:   "120",
			expectedFinnhubWSURL:        "wss://custom-ws-url",
			expectedFinnhubAPIKey:       "custom-api-key",
			expectedFinnhubTradeWinSize: 120,
			setEnvVariable:              true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.setEnvVariable {
				os.Setenv("FINNHUB_WEBSOCKET_URL", tt.envFinnhubWSURL)
				os.Setenv("FINNHUB_API_KEY", tt.envFinnhubAPIKey)
				os.Setenv("FINNHUB_TRADE_WINDOW_SIZE", tt.envFinnhubTradeWindowSize)
				defer func() {
					os.Unsetenv("FINNHUB_WEBSOCKET_URL")
					os.Unsetenv("FINNHUB_API_KEY")
					os.Unsetenv("FINNHUB_TRADE_WINDOW_SIZE")
				}()
			}

			conf, err := New()
			if err != nil {
				t.Fatalf("Unexpected error: %v", err)
			}

			if conf.FinnhubWebSocketURL != tt.expectedFinnhubWSURL {
				t.Errorf("Unexpected FinnhubWebSocketURL, got: %s, want: %s", conf.FinnhubWebSocketURL, tt.expectedFinnhubWSURL)
			}

			if conf.FinnhubApiKey != tt.expectedFinnhubAPIKey {
				t.Errorf("Unexpected FinnhubApiKey, got: %s, want: %s", conf.FinnhubApiKey, tt.expectedFinnhubAPIKey)
			}

			if conf.FinnhubTradeWindowSize != tt.expectedFinnhubTradeWinSize {
				t.Errorf("Unexpected FinnhubTradeWindowSize, got: %d, want: %d", conf.FinnhubTradeWindowSize, tt.expectedFinnhubTradeWinSize)
			}
		})
	}
}
