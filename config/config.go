package config

import (
	"gitlab.com/challenges/fun-with-channels/v2/envutil"
)

type Config struct {
	FinnhubWebSocketURL    string
	FinnhubApiKey          string
	FinnhubTradeWindowSize int
	DBURL                  string
}

const (
	DefaultFinnHubWSURL           = "wss://ws.finnhub.io"
	DefaultFinnhubApiKey          = "incorrect-api-key"
	DefaultFinnhubTradeWindowSize = 60
	defaultDBURL                  = "host=postgres user=docker password=docker dbname=docker sslmode=disable"
)

// New creates a new Config instance with default values or
// values obtained from environment variables.
func New() (*Config, error) {

	finnhubWSURL := envutil.GetEnvOptionalString("FINNHUB_WEBSOCKET_URL", DefaultFinnHubWSURL)
	FinnhubApiKey := envutil.GetEnvOptionalString("FINNHUB_API_KEY", DefaultFinnhubApiKey)
	FinnhubTradeWindowSize := envutil.GetEnvOptionalInt("FINNHUB_TRADE_WINDOW_SIZE", DefaultFinnhubTradeWindowSize)
	dbURL := envutil.GetEnvOptionalString("DATABASE_URL", defaultDBURL)
	return &Config{
		FinnhubWebSocketURL:    finnhubWSURL,
		FinnhubApiKey:          FinnhubApiKey,
		FinnhubTradeWindowSize: FinnhubTradeWindowSize,
		DBURL:                  dbURL,
	}, nil
}
