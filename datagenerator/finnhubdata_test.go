package datagenerator

import (
	"fmt"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"gitlab.com/challenges/fun-with-channels/v2/cryptomodel"
	"gitlab.com/challenges/fun-with-channels/v2/repo"
)

func TestReadAndWriteValue(t *testing.T) {
	testCases := []struct {
		name           string
		initialValues  []float64
		trade          cryptomodel.FinnhubTradeData
		windowSize     int
		expectedValues []float64
	}{
		{
			name:          "Add BTCUSDT value within window size",
			initialValues: []float64{2.0, 5.0, 3.0},
			trade: cryptomodel.FinnhubTradeData{
				Price:  6.0,
				Symbol: "BINANCE:BTCUSDT",
			},
			windowSize:     4,
			expectedValues: []float64{2.0, 5.0, 3.0, 6.0},
		},
		{
			name:          "Add ETHUSDT value within window size",
			initialValues: []float64{4.0, 5.0, 3.0},
			trade: cryptomodel.FinnhubTradeData{
				Price:  4.0,
				Symbol: "BINANCE:ETHUSDT",
			},
			windowSize:     4,
			expectedValues: []float64{4.0, 5.0, 3.0, 4.0},
		},
		{
			name:          "Add value more than window size",
			initialValues: []float64{1.0, 2.0, 3.0},
			trade: cryptomodel.FinnhubTradeData{
				Price:  4.0,
				Symbol: "BINANCE:ETHUSDT",
			},
			windowSize:     2,
			expectedValues: []float64{3.0, 4.0},
		},
		{
			name:          "Add value with window size 1",
			initialValues: []float64{1.0},
			trade: cryptomodel.FinnhubTradeData{
				Price:  2.0,
				Symbol: "BINANCE:ETHUSDT",
			},
			windowSize:     1,
			expectedValues: []float64{2.0},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			cryptoValues := make(map[string]FinnHubPriceUpdates)
			cryptoValues[testCase.trade.Symbol] = FinnHubPriceUpdates{
				Values: testCase.initialValues,
			}
			db, _, err := sqlmock.New()
			if err != nil {
				t.Fatal(err)
			}
			defer db.Close()
			repo := repo.NewFinnhubRepository(db)
			smaGenerator := &FinnHubDataGenerator{
				CryptoValues: cryptoValues,
				Repo:         repo,
				WindowSize:   testCase.windowSize,
			}

			smaGenerator.readAndWriteFinnHubPriceUpdate(testCase.trade)

			values := smaGenerator.CryptoValues[testCase.trade.Symbol].Values
			if !areFloatArraysEqual(values, testCase.expectedValues) {
				t.Errorf("Unexpected values after adding. Got %v, want %v", values, testCase.expectedValues)
			}
		})
	}
}

func areFloatArraysEqual(arr1, arr2 []float64) bool {
	if len(arr1) != len(arr2) {
		return false
	}
	for i := range arr1 {
		if arr1[i] != arr2[i] {
			return false
		}
	}
	return true
}

func TestCalculateSMA(t *testing.T) {
	testCases := []struct {
		name          string
		initialValues []float64
		symbol        string
		windowSize    int
		expectedSMA   float64
		expectedErr   error
	}{
		{
			name:          "Valid calculation",
			initialValues: []float64{1.0, 2.0, 3.0, 4.0, 5.0},
			symbol:        "BINANCE:ETHUSDT",
			windowSize:    1,
			expectedSMA:   5.0,
			expectedErr:   nil,
		},
		{
			name:          "Not enough values",
			initialValues: []float64{1.0, 2.0},
			symbol:        "BINANCE:ETHUSDT",
			windowSize:    3,
			expectedSMA:   0.0,
			expectedErr:   fmt.Errorf("values count is less than window size for SMA calculation"),
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			cryptoValues := make(map[string]FinnHubPriceUpdates)
			cryptoValues[testCase.symbol] = FinnHubPriceUpdates{
				Values: testCase.initialValues,
			}
			db, _, err := sqlmock.New()
			if err != nil {
				t.Fatal(err)
			}
			defer db.Close()
			repo := repo.NewFinnhubRepository(db)
			smaGenerator := &FinnHubDataGenerator{
				CryptoValues: cryptoValues,
				Repo:         repo,
				WindowSize:   testCase.windowSize,
			}

			priceUpdates := smaGenerator.CryptoValues[testCase.symbol]
			result, err := smaGenerator.CalculateSMA(priceUpdates.Values)

			if result != testCase.expectedSMA {
				t.Errorf("Unexpected SMA result.Crypto %s, Got %f, want %f", testCase.symbol, result, testCase.expectedSMA)
			}

			if (err == nil && testCase.expectedErr != nil) || (err != nil && err.Error() != testCase.expectedErr.Error()) {
				t.Errorf("Unexpected error. Got %v, want %v", err, testCase.expectedErr)
			}
		})
	}
}
