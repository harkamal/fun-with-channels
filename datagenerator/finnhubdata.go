package datagenerator

import (
	"database/sql"
	"fmt"
	"log"

	"gitlab.com/challenges/fun-with-channels/v2/cryptomodel"
	"gitlab.com/challenges/fun-with-channels/v2/repo"
)

type FinnHubDataGenerator struct {
	CryptoValues map[string]FinnHubPriceUpdates
	WindowSize   int
	Repo         repo.FinnhubRepository
}

type FinnHubPriceUpdates struct {
	Values      []float64
	LatestPrice float64
	LatestSMA   float64
	Timestamp   int64
}

func NewFinnhubDataGenerator(windowSize int, db *sql.DB) *FinnHubDataGenerator {
	repo := repo.NewFinnhubRepository(db)
	return &FinnHubDataGenerator{
		WindowSize:   windowSize,
		Repo:         repo,
		CryptoValues: make(map[string]FinnHubPriceUpdates),
	}
}

func (s *FinnHubDataGenerator) readPriceUpdate(symbol string) FinnHubPriceUpdates {
	return s.CryptoValues[symbol]
}

func (s *FinnHubDataGenerator) writePriceUpdate(symbol string, FinnHubPriceUpdates FinnHubPriceUpdates) FinnHubPriceUpdates {
	s.CryptoValues[symbol] = FinnHubPriceUpdates
	return FinnHubPriceUpdates
}

// ProcessFinnhubTradeData process real time trade updates.
func (s *FinnHubDataGenerator) ProcessFinnhubTradeData(
	tradeDataChannel <-chan cryptomodel.FinnhubTradeData,
) {
	for tradeData := range tradeDataChannel {
		s.readAndWriteFinnHubPriceUpdate(tradeData)
	}
}

func (s *FinnHubDataGenerator) readAndWriteFinnHubPriceUpdate(tradeData cryptomodel.FinnhubTradeData) {
	FinnHubPriceUpdates := s.readPriceUpdate(tradeData.Symbol)
	values := FinnHubPriceUpdates.Values
	values = append(values, tradeData.Price)
	if len(values) > s.WindowSize {
		values = values[(len(values) - s.WindowSize):]
	}
	FinnHubPriceUpdates.Values = values
	FinnHubPriceUpdates.LatestPrice = tradeData.Price
	s.writePriceUpdate(tradeData.Symbol, FinnHubPriceUpdates)

	log.Printf("last price receieved at %d, price: %f", tradeData.Timestamp, tradeData.Price)

	if len(FinnHubPriceUpdates.Values) >= s.WindowSize {
		smaValue, err := s.CalculateSMA(FinnHubPriceUpdates.Values)
		if err != nil {
			log.Println("Error calculating SMA:", err, tradeData.Symbol)
			return
		}

		s.Repo.InsertMovingAverage(smaValue, tradeData)
		FinnHubPriceUpdates.LatestSMA = smaValue
		FinnHubPriceUpdates.Timestamp = tradeData.Timestamp
		s.writePriceUpdate(tradeData.Symbol, FinnHubPriceUpdates)
	}
}

// CalculateSMA calculates the simple moving average.
func (s *FinnHubDataGenerator) CalculateSMA(prices []float64) (float64, error) {
	if len(prices) < s.WindowSize {
		return 0.0, fmt.Errorf("values count is less than window size for SMA calculation")
	}

	sum := 0.0
	for _, value := range prices[(len(prices) - s.WindowSize):] {
		sum += value
	}

	return sum / float64(s.WindowSize), nil
}
